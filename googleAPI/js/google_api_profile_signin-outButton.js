const apiKey= 'AIzaSyDOSHeSoOqA7HsxHgj7JAUgefeDTmnYkAk';
const clientId= '738333621007-jgt09vbk6c8mm7qgjdreu2n8hhtq03l4.apps.googleusercontent.com';
const scopes= 'profile';


function handleClientLoad() {
        // Load the API client and auth library
        gapi.load('client:auth2', initAuth);
}

function initAuth() {
    gapi.auth2.init({
        client_id: clientId,
        scope: scopes
    }).then(function() {
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        signinButton.addEventListener("click", signin);
        signoutButton.addEventListener("click", signout);
    });
}

function updateSigninStatus(isSignedIn) {
    if(isSignedIn) {
        signinButton.style.display = 'none';
        signoutButton.style.display = 'block';
    } else {
        signinButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

function signin(event) {
    gapi.auth2.getAuthInstance().signIn();
}

function signout(event) {
    gapi.auth2.getAuthInstance().signOut();
}

// De fout dat ik ben tegengekomen in firefox is uncaught exception: [object Object]. In Chrome kreeg ik wat extra informatie.
// Heel kort was die informatie dat de origin van mijn client_id dus deze URL https://mehdisahri-15301-msahri.c9users.io niet
// in de whitelist stond. Om dit op te lossen:
// 1. Naar http://console.developers.google.com/ gaan. 
// 2. API Key bewerken.
// 3. Bij Application restrictions HTTP referrers (web sites) kiezen.
// 4. Bij Accept requests from these HTTP referrers (web sites) site ingeven. (bij mij: https://mehdisahri-15301-msahri.c9users.io)
// 5. Oplsaan.
// 6. 5 minuten wachten.
// 7. Zou in orde moetn zijn.

