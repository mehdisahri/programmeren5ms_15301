// Set the map variable
const myMap = L.map('map');

// Load the basemap
const myBasemap = L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '© <a href="//www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

// Add basemap to map id
myBasemap.addTo(myMap);

// Set view of the map
// myMap.setView([41.939948, -87.650673], 12);

// Set view of the map
myMap.setView([51.1698497, 4.361586699999975], 12);

let request = new XMLHttpRequest();
request.open('get', 'data/organisation.json', true);
request.onload = function() {
    const organisationList = JSON.parse(this.response)
    // alert(this.response);
    organisationList.map(organisation => {
        L.marker([organisation.latitude, organisation.longitude]).bindPopup(`
            <h2>${organisation.name}</h2>
            <p><b>School:</b> ${organisation.name}</p>
        `).openPopup().addTo(myMap);
        });
}

request.send();

