var apiKey = 'AIzaSyA1eJH7HG9SOyP2Jxzt3_c6UKbtCYgEY8A';
var clientId = '464946994907-k7ei824kbm8c1jk9egvuuqcmlsqthabn.apps.googleusercontent.com';
var scopes = 'profile';

var signinButton = document.getElementById('signin-button');
var signoutButton = document.getElementById('signout-button');

function loadGoogleAPI() {
    // Load the API client and auth library
    gapi.load('client:auth2', initAuth);
}

function initAuth() {
    // gapi.client.setApiKey(apiKey);
    gapi.auth2.init({
            'apiKey': apiKey,
            client_id: clientId,
            scope: scopes
        }).then(function() {
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);
            // Handle the initial sign-in state.
            updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
            signinButton.addEventListener("click", signin);
            signoutButton.addEventListener("click", signout);
        }).then(makePeopleApiCall)
        .then(showUser);
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        signinButton.style.display = 'none';
        signoutButton.style.display = 'block';
        // makeApiCall();
    }
    else {
        signinButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

function signin(event) {
    gapi.auth2.getAuthInstance().signIn();
}

function signout(event) {
    gapi.auth2.getAuthInstance().signOut();
}

// Load the API and make an API call.  Display the results on the screen.
function makePeopleApiCall() {
    gapi.client.load('people', 'v1', function() {
        var request = gapi.client.people.people.get({
            resourceName: 'people/me',
            'requestMask.includeField': 'person.names'
        });
        request.execute(function(resp) {
            var p = document.createElement('p');
            if (resp.names) {
                var name = resp.names[0].givenName;
            }
            else {
                var name = 'Geen naam gevonden';
            }
            p.appendChild(document.createTextNode('Hello, ' + name + '!'));
            document.getElementById('content').appendChild(p);
            // Toon het response object als JSON string
            var pre = document.createElement('pre');
            var feedback = JSON.stringify(resp, null, 4);
            pre.appendChild(document.createTextNode(feedback));
            document.getElementById('content').appendChild(pre);
        });
    });
}

function showUser(resp) {
    // Note: In this example, we use the People API to get the current
    // user's name. In a real app, you would likely get basic profile info
    // from the GoogleUser object to avoid the extra network round trip.
    var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    var h1 = document.createElement('h1');
    h1.appendChild(document.createTextNode(profile.getId()));
    document.getElementById('content').appendChild(h1);
    var h2 = document.createElement('h2');
    h2.appendChild(document.createTextNode(profile.getName()));
    document.getElementById('content').appendChild(h2);
    var h3 = document.createElement('h3');
    h3.appendChild(document.createTextNode(profile.getGivenName()));
    document.getElementById('content').appendChild(h3);
    var h4 = document.createElement('h4');
    h4.appendChild(document.createTextNode(profile.getFamilyName()));
    document.getElementById('content').appendChild(h4);
    var img = document.createElement('img');
    img.setAttribute("src", profile.getImageUrl());
    document.getElementById('content').appendChild(img);
    var h5 = document.createElement('h5');
    h5.appendChild(document.createTextNode(profile.getEmail()));
    document.getElementById('content').appendChild(h5);
}
