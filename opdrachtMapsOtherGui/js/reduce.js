// Set the map variable
const myMap = L.map('map');

// Load the basemap
const myBasemap = L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '© <a href="//www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

// Add basemap to map id
myBasemap.addTo(myMap);

// Set view of the map
// myMap.setView([41.939948, -87.650673], 12);

// Set view of the map
myMap.setView([51.2682227, 4.712569400000007], 10);

let request = new XMLHttpRequest();
request.open('get', 'data/codeHomes.json', true);
request.onload = function() {
    const codeHomesList = JSON.parse(this.response)
    const kindOfCity =  codeHomesList.reduce((sums, codeHomes) => {
        sums[codeHomes.city] = (sums[codeHomes.city] || 0) + 1;
        return sums;
    }, {});

    const sidebar = document.getElementById('city');
    const h3 = document.createElement("h3");
    h3.innerHTML = "Code Homes per city";
    h3.style.fontWeight = "bold";
    sidebar.appendChild(h3);

    for (let city in kindOfCity) {
      const p = document.createElement("p");
        p.innerHTML = `<b>${city}</b> : ${kindOfCity[city]} CodeHomes`;
        sidebar.appendChild(p);
    }
    
    // alert(this.response);
    codeHomesList.map(codeHomes => {
        L.marker([codeHomes.latitude, codeHomes.longitude]).bindPopup(`
        <div class="infoContainer">   
            <div class="info">    
                <img src="${codeHomes.image}" alt="${codeHomes.name} pic"> 
                <h5><b>Talen:</b> ${codeHomes.proglanguage}</h5>
                <h5><b>Adres:</b> ${codeHomes.street}, ${codeHomes.postalcode} ${codeHomes.city} - Belgium</h5>
                <h5><b>Telefoon:</b> ${codeHomes.phone}</h5>
                <h5><b>Email:</b> ${codeHomes.email}</h5>
            </div>  
        </div>    
        `).openPopup().addTo(myMap);
        });

    const buttonBack = document.createElement("A");
    buttonBack.href = "index.html";
    buttonBack.id = "backRedBtn";
    buttonBack.unselectable = "on";
    buttonBack.classList.add("btn-primary");
    buttonBack.classList.add("btn-lg");
    buttonBack.text = "Index";
    sidebar.appendChild(buttonBack);
    }    
request.send();