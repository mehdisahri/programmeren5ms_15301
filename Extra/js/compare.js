<script>
    function compare(a, b){
          let comparison = 0;
        
          if (a > b) {
            comparison = 1;
          } else if (b > a) {
            comparison = -1;
          }
        
          return comparison;
        }
        
        const foo = [9, 2, 3, 'random', 'panda'];
        // for (let i = 0; i < foo.length; i++) {
        //     foo[i]
        // }
        document.getElementById('output').innerHTML=
            `array bevat in volgorde: ${foo[0]}, ${foo[1]}, ${foo[2]}, ${foo[3]}, ${foo[4]} <br>`;
        foo.sort(); // returns [ 2, 3, 9, 'panda', 'random' ]
        document.getElementById('output').innerHTML +=
            `geordende array bevat in volgorde: ${foo[0]}, ${foo[1]}, ${foo[2]}, ${foo[3]}, ${foo[4]} <br>`;
            
        const bar = [4, 19, 30, function(){}, {key: 'value'}];
        bar.sort(compare);
            
        document.getElementById('output').innerHTML +=
            `geordende array bevat in volgorde: ${bar[0]}, ${bar[1]}, ${bar[2]}, ${bar[3]}, ${bar[4]}`;
    </script>