let person = `[
    {
      "api": "Identity",
      "code": "ID",
      "title": "Identiteit",
      "description": "Persoonlijke gegevens",
      "version": "0.0",
      "updated": "2016-11-16 13:33:42",
      "role": "LDO",
      "userName" : "jing",
      "password" : "123456",
      "firstName": "Joseph",
      "lastName": "Inghelbrecht",
      "phoneWork": "0486 78 87 23",
      "phoneHome": "0486 78 87 23",
      "mobile": "0486 78 87 23",
      "email": "jef.inghelbrecht@telenet.be",
      "street": "Braziliëstraat 38",
      "postalCode": "2000",
      "city": "Antwerpen",
      "function": "Docent"
    },
    {
      "api": "Identity",
      "code": "ID",
      "title": "Identiteit",
      "description": "Persoonlijke gegevens",
      "version": "0.0",
      "updated": "2016-11-16 13:33:42",
      "role": "STAF",
      "userName" : "guy",
      "password" : "123456",
      "firstName": "Guy",
      "lastName": "Linten",
      "phoneWork": "0486 78 87 23",
      "phoneHome": "0486 78 87 23",
      "mobile": "0486 78 87 23",
      "email": "guy.linten@telenet.be",
      "street": "Ergensstraat 2",
      "postalCode": "1000",
      "city": "Brussel",
      "function": "Docent"
    },
    {
      "api": "Identity",
      "code": "ID",
      "title": "Identiteit",
      "description": "Persoonlijke gegevens",
      "version": "0.0",
      "updated": "2016-11-16 13:33:42",
      "role": "DV",
      "userName" : "dvjing",
      "password" : "123456",
      "firstName": "Joseph",
      "lastName": "Inghelbrecht",
      "phoneWork": "0486 78 87 23",
      "phoneHome": "0486 78 87 23",
      "mobile": "0486 78 87 23",
      "email": "jef.inghelbrecht@telenet.be",
      "street": "Braziliëstraat 38",
      "postalCode": "2000",
      "city": "Antwerpen",
      "function": "Docent/Directe verantwoordelijke"
    },
    {
      "api": "Identity",
      "code": "ID",
      "title": "Identiteit",
      "description": "Persoonlijke gegevens",
      "version": "0.0",
      "updated": "2016-11-16 13:33:42",
      "role": "DV",
      "userName" : "dvjing",
      "password" : "123456",
      "firstName": "Joseph",
      "lastName": "Inghelbrecht",
      "phoneWork": "0486 78 87 23",
      "phoneHome": "0486 78 87 23",
      "mobile": "0486 78 87 23",
      "email": "jef.inghelbrecht@telenet.be",
      "street": "Braziliëstraat 38",
      "postalCode": "2000",
      "city": "Antwerpen",
      "function": "Secretariaat"
    },
    {
      "api": "Identity",
      "code": "ID",
      "title": "Identiteit",
      "description": "Persoonlijke gegevens",
      "version": "0.0",
      "updated": "2016-11-16 13:33:42",
      "role": "DIR",
      "userName" : "dirjing",
      "password" : "123456",
      "firstName": "Joseph",
      "lastName": "Inghelbrecht",
      "phoneWork": "0486 78 87 23",
      "phoneHome": "0486 78 87 23",
      "mobile": "0486 78 87 23",
      "email": "jef.inghelbrecht@telenet.be",
      "street": "Braziliëstraat 38",
      "postalCode": "2000",
      "city": "Antwerpen",
      "function": "Directeur"
    }
  ]`

const peopleList = JSON.parse(person);


for (let i = 0; i < peopleList.length; i++) {
  let workFunction = peopleList[i].function;
  
    document.body.appendChild(createvCard(peopleList[i].firstName,
        peopleList[i].lastName,
        peopleList[i].street,
        peopleList[i].postalCode,
        peopleList[i].city,
        peopleList[i].email,
        peopleList[i].mobile,
        workFunction))
}

function createvCard (firstName, lastName, street, postalCode, city, email, mobile, workFunction) {
    let vcard = document.createElement('DIV');
    vcard.className = "tile";
    vcard.appendChild(createTextElement('P', firstName + ' ' + lastName + ', ' + workFunction, 'given-name'));
    let address = document.createElement('DIV');
    address.className = 'adr';
    address.appendChild(createTextElement('DIV', street, 'street-address'));
    address.appendChild(createTextElement('DIV', postalCode, 'postal-code'));
    address.appendChild(createTextElement('DIV', city, 'locality'));
    vcard.appendChild(address);
    let info = document.createElement('DIV');
    info.className = 'info';
    info.appendChild(createTextElement('DIV', email, 'email'));
    info.appendChild(createTextElement('DIV', mobile, 'tel'));
    vcard.appendChild(info);
    return vcard;
}

function createTextElement(tag, text, className) {
    let element = document.createElement(tag);        // Create an 'tag' element
    var textNode = document.createTextNode(text);   // Create a text node
    element.appendChild(textNode); // Append the text to elem
    element.style.className = className;
    return element;                    
}


